# Backend 

## <b>Резпозитории</b>

1) ### <a href="https://gitlab.com/hackKinbery/hackkinbery">Backend</a> - node js сервер 
2) ### <a href="https://gitlab.com/hackKinbery/frontend">Frontend</a> - веб сервис для друзей
3) ### <a href="https://gitlab.com/hackKinbery/mobile">Mobile</a> - мобильный микросервис для поиска ребёнка
4) ### <a href="https://gitlab.com/hackKinbery/kinberyjoint">Описание проекта</a>  
5) ### <a href="https://gitlab.com/hackKinbery/model">Моделирование положения Python</a>  

<h1>Install requires </h1>
1. <b> postgres 13</b> with backup in <a href="https://gitlab.com/hackKinbery/hackkinbery/-/tree/master/backups">backup</a> folder<br>
 2. install all <b>node modules </b><br>
 3. do not forget about config for db in<b> <i>./config/default.json</i></b><br>

## > ``` npm install ```

if it's prod ? ok use this
## > ``` npm run server ```

if it's local ? ok use this
## > ``` npm run server:local ```

# Stack
```
- node.js, express.js
- postgres 13
- swagger
```
# Swagger

``` node ./swagger/app.js > on localhost:10100 ```

# API 

## user data

- поулчить пользователя по телефону <br>
``` GET /api/userData/getUserIdByPhone/:phone ``` 

- получить пользователя по мак адресу <br> 
``` GET /api/userData/getUserIdByMac/:mac ``` 

- сохранить данные о пользователе в виде {userId, mac, phone} <br>
``` POST /api/userData/saveUserData ```  

- получить даныне пользователя по сопоставлению monitor<br>
``` GET /api/userData/getMacById/:parentId/:childId ```

- получить все мониторы по userId <br>
``` GET /api/userData/getMonitorByParentId/:parentId ``` 

- занести новый мониторинг для пользователя парент id <br>
``` PUT /api/userData/getMonitorByParentId/:parentId/:childId ``` 

## bluetooth

- закрыть запрос на поиск по блютузу когда телефон найден<br>
``` DELETE /api/bluetooth/iFoundBluetoothRequest/:requestId => returns code 201 ```

- получить инфу пльзователя по мак адресу<br>
``` GET /api/bluetooth/getBluetoothUserInfo/:mac => returns {} ```

- получить информацию пользователя по его id<br>
``` GET /api/bluetooth/getBluetoothUserInfo/:userId => {userId,phone,mac} ```

- по мак адрессу получить запрашивали его на поиск<br>
``` GET /api/bluetooth/getBluetoothRequest/:mac ``` 

## location

- сохранить последнюю позицию ответа по блютузу<br>
``` POST /api/location/saveBluetoothLocation -> body = {mac, lat, long} => returns 201 ``` <br>
``` POST /api/location/saveUserLocationByMac -> body = {mac, lat, long} => returns 201 ``` <br>

- сохранить последние координаты пользователя<br>
``` POST /api/location/saveUserLocation  -> body {userId, lat, long, battery} => returns 201 ```<br>
``` GET /api/location/saveUserLocation/:userId/:lat/:long/:battery/:date ```<br>
``` GET /api/location/saveUserLocation/:userId/:lat/:long/:battery ```<br>
``` POST /api/location/saveUserLocations (массив)```<br>

- по маку получить где был последний раз этот пользователь (блютуз)<br>
``` GET /api/location/getBluetoothLocations/:mac ``` 

- трек childId за день<br>
``` GET /api/location/getChildTrackByParentId/:parentId/:childId ```

- получить последнюю позицию childId<br>
``` GET /api/location/getChildLastCoordinatesByGps/:parentId/:childId ``` 

- поулчить последнюю позицию по мобильной вышке<br>
``` GET /api/location/getTrackByCell/:parentId/:childId ```

- получить последнюю позицию child по мак адресу<br>
``` GET /api/location/getLastCoordinatesByBT/:parentId/:childId ```

- последняя позиция по мобильной вышке<br>
``` GET /api/location/getLastCoordinatesByCell/:parentId/:childId ```

- получить последнюю координату childId 
``` GET /api/location/getChildLastCoordinatesByCell/:childId ```

## request

- удовлетворить запрос поиска и закрыть его <br>
``` GET /api/request/iFoundRequest/:requestId ```

- создать запрос на поиск childId<br>
``` GET /api/request/createChildRequest/:parentId/:childId ``` 

- создать запрос на поиск childId по маку<br>
``` GET /api/request/createRequestBtToChild/:parentId/:childId ```

- поулчение запросов на поиск пользователя<br>
``` GET /api/request/getRequestsByUserId/:userId ```

- удалить запрос поиска parentId <br>
``` DELETE /api/request/clearRequest/:parentId/:childId ```

- удалить запрос поиска по блютузу <br>
``` DELETE /api/request/clearRequestBt/:parentId/:childId ```

## friends

- сохранить данные друга<br>
``` GET /api/friends/saveUserFriend/:userId/:friendId ```

- удалить данные друга<br>
``` DELETE /api/friends/deleteUserFriend/:userId/:friendId ```

- найти по расположению друзей<br>
``` GET /api/friends/findInFriendsLocation/:userId ```

## monitor

- получить все мониторинги пользователя <br>
``` GET /api/monitor/getParentMonitors/:parentId ```

- внесение нового мониторинга <br>
``` POST /api/monitor/saveMonitoring ```

- получить все мониторинги childId <br>
``` GET /api/monitor/getChildMonitors/:childId ```

## routes

- получить все маршруты childId<br>
``` GET /api/childRoutes/getChildRoutes/:childId ```

- создать новую станцию<br>
``` GET /api/childRoutes/saveNewRoute/:childId/:name ``` 

- сохранить станцию в маршруте<br>
``` GET /api/childRoutes/saveStation/:routeId/:lat/:lng/:number ``` 

- получить санции по заданному маршруту <br>
``` GET /api/childRoutes/getStationByRouteId/:routeId ``` 
