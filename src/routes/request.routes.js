const {Router} = require('express');
import {db, wrapResponse, handleDefault} from '../utils';
const router = Router();

router.get(
    '/getRequestsByUserId/:userId',
    wrapResponse((request, response) => {
        const userId = request.params.userId;
        console.log(`Определяем, был ли сделан запрос на местоположение человека с userId=${userId}`)
        if(userId && Number.isInteger(Number(userId))) {
            request.pool
                .query(db.queries.select('request', "communication", {user_id: userId}))
                .then(db.getOne)
                .then((result) => {
                    return response.json(result)
                });
        } else {
            return response.status(400).json({ message: "Can not get userId: " + userId });
        }
    })
);



router.delete(
    '/iFoundRequest/:requestId',
    wrapResponse((request, response) => {
        const requestId = request.params.requestId;
        if(requestId) {
            request.pool
                .query(db.queries.delete('request', "communication",{id: requestId}))
                .then(() => response.status(201).json({ message: "Данные для получения запроса удалены", requestId: requestId }));
        } else {
            return response.status(400).json({ message: "Can not iFoundRequest, requestId: " + requestId });
        }
    })
);

router.delete(
    '/clearRequest/:parentId/:childId',
    wrapResponse((request, response) => {
        const parentId = request.params.parentId;
        const childId = request.params.childId;
        if(parentId && childId) {
            request.pool
                .query(db.queries.clearRequest(parentId, childId))
                .then(() => response.status(201).json({ message: "Данные запроса на поиск удалены"}));
        } else {
            return response.status(400).json({ message: "Can not clearRequest, parentId: " + parentId });
        }
    })
);

router.delete(
    '/clearRequestBt/:parentId/:childId',
    wrapResponse((request, response) => {
        const parentId = request.params.parentId;
        const childId = request.params.childId;
        if(parentId && childId) {
            request.pool
                .query(db.queries.clearRequestBt(parentId, childId))
                .then(() => response.status(201).json({ message: "Данные запроса на поиск по блюутузу удалены"}));
        } else {
            return response.status(400).json({ message: "Can not clearRequestBt, parentId: " + parentId });
        }
    })
);

router.get(
    '/createChildRequest/:parentId/:childId',
    wrapResponse((request, response) => {
        const childId = request.params.childId;
        const parentId = request.params.parentId;
        if(childId && parentId) {
            request.pool
                .query(db.queries.createRequestToChild(parentId, childId))
                .then(db.getAll)
                .then((result) => response.json(result));
        } else {
            return response.status(400).json({ message: "createChildRequest is invalid"});
        }
    })
);

router.get(
    '/createRequestBtToChild/:parentId/:childId',
    wrapResponse((request, response) => {
        const childId = request.params.childId;
        const parentId = request.params.parentId;
        if(childId && parentId) {
            request.pool
                .query(db.queries.createRequestBtToChild(parentId, childId))
                .then(db.getAll)
                .then((result) => response.json(result));
        } else {
            return response.status(400).json({ message: "createRequestBtToChild is invalid"});
        }
    })
);



module.exports = router;
