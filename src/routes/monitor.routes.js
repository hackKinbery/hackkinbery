const {Router} = require('express');
import {db, wrapResponse, handleDefault} from '../utils';
const router = Router();


router.get(
    '/getParentMonitors/:parentId',
    wrapResponse((request, response) => {
        const parentId = request.params.parentId;
        if(parentId) {
            request.pool
                .query(db.queries.select('monitor', "user_data", {parent_id: parentId}))
                .then(db.getAll)
                .then((result) => {
                    return response.json(result)
                });
        } else {
            return response.status(400).json({ message: "Can not getParentMonitors with parentId: " + parentId });
        }
    })
);

router.get(
    '/getChildMonitors/:childId',
    wrapResponse((request, response) => {
        const childId = request.params.childId;
        if(childId) {
            request.pool
                .query(db.queries.select('monitor', "user_data", {child_id: childId}))
                .then(db.getAll)
                .then((result) => {
                    return response.json(result)
                });
        } else {
            return response.status(400).json({ message: "Can not getChildMonitors with parentId: " + childId });
        }
    })
);

router.post(
    '/saveMonitoring',
    wrapResponse((request, response) => {
        const parentMonitoring = request.body;
        console.log(parentMonitoring)
        if(parentMonitoring && parentMonitoring.parentId && parentMonitoring.childId) {
            request.pool
                .query(db.queries.insert('monitor', "user_data", {
                    parent_id: parentMonitoring.parentId,
                    child_id: parentMonitoring.childId
                }))
                .then(db.getAll)
                .then(() => response.status(201).json({message: "Данные мониторинга сохранены", parentId: parentMonitoring.parentId}));
        } else {
            return response.status(400).json({ message: "Can not saveMonitoring"});
        }
    })
);

router.post(
    '/applyMonitoring',
    wrapResponse((request, response) => {
        //todo apply current monitoring validation
    })
);




module.exports = router;