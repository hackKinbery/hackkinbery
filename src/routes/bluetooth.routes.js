const {Router} = require('express');
import {db, wrapResponse, handleDefault, isMacAddress} from '../utils';
const router = Router();

// GET /api/bluetooth/getBluetoothUserInfo/:userId
router.get(
    '/getBluetoothUserInfo/:userId',
    wrapResponse((request, response) => {
        const userId = request.params.userId;
        if(Number.isInteger(Number(userId))) {
            request.pool
                .query(db.queries.select('user_data', "user_data", {user_id: userId}))
                .then(db.getAll)
                .then((result) => response.json(result));
        } else {
            return response.status(400).json({ message: "Can not get user with id: " + userId });
        }
    })
);

// GET /api/bluetooth/getBluetoothRequest/:mac
router.get(
    '/getBluetoothRequest/:mac',
    wrapResponse((request, response) => {
        const mac = request.params.mac;
        console.log(`Было обнаружено устройство c UUID=${mac}. Определяем, есть ли запрос на его местоположение..`)
        if(mac && isMacAddress(mac)) {
            request.pool
                .query(db.queries.select('request_bt', "communication", {mac: mac}))
                .then(db.getAll)
                .then((result) => response.json((result && Array.isArray(result) && result.length>0)));
        } else {
            return response.status(400).json({ message: "Can not get mac: " + mac });
        }
    })
);

// DELETE /api/bluetooth/iFoundBluetoothRequest/:requestId
router.delete(
    '/iFoundBluetoothRequest/:requestId',
    wrapResponse((request, response) => {
        const requestId = request.params.requestId;
        if(requestId) {
            request.pool
                .query(db.queries.delete('request_bt', "communication",{id: requestId}))
                .then(() => response.status(201).json({ message: "Данные Bluetooth удалены", requestId: requestId }));
        } else {
            return response.status(400).json({ message: "Can not delete requestId: " + requestId });
        }
    })
);



module.exports = router;
