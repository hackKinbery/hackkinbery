const {Router} = require('express');
import {db, wrapResponse, handleDefault} from '../utils';
const router = Router();

router.get(
    '/saveNewRoute/:childId/:name',
    wrapResponse((request, response) => {
        const childId = request.params.childId;
        const name = request.params.name;
        if(childId && name) {
            request.pool
                .query(db.queries.insert('child_route', "user_data", {user_id: childId, name: name}))
                .then(db.getOne)
                .then((result) => {
                    return response.json(result)
                });
        } else {
            return response.status(400).json({ message: "Can not getChildRoutes with childId: " + childId });
        }
    })
);

router.get(
    '/saveStation/:routeId/:lat/:lng/:number',
    wrapResponse((request, response) => {
        const route_id  = request.params.routeId;
        const lng = request.params.lng;
        const lat = request.params.lat;
        const number = request.params.number;
        if(route_id && lng && lat && number) {
            request.pool
                .query(db.queries.insert('route_station', "user_data", {lat: lat, long: lng, route_id: route_id, number: number}))
                .then(db.getOne)
                .then((result) => {
                    return response.json(result)
                });
        } else {
            return response.status(400).json({ message: "Can not saveStation with routeId: " + routeId });
        }
    })
);


router.get(
    '/getChildRoutes/:childId',
    wrapResponse((request, response) => {
        const childId = request.params.childId;
        if(childId) {
            request.pool
                .query(db.queries.select('child_route', "user_data", {parent_id: childId}))
                .then(db.getAll)
                .then((result) => {
                    return response.json(result)
                });
        } else {
            return response.status(400).json({ message: "Can not getChildRoutes with childId: " + childId });
        }
    })
);

router.get(
    '/getStationByRouteId/:routeId',
    wrapResponse((request, response) => {
        const routeId = request.params.routeId;
        if(routeId) {
            request.pool
                .query(db.queries.select('route_station', "user_data", {route_id: routeId},
                    null, null, "order by t.Number"))
                .then(db.getAll)
                .then((result) => {
                    return response.json(result)
                });
        } else {
            return response.status(400).json({ message: "Can not getStationByRouteId with routeId: " + routeId });
        }
    })
);




module.exports = router;