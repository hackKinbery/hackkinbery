const {Router} = require('express');
import {db, wrapResponse, handleDefault, isMacAddress} from '../utils';
const router = Router();
let Promise = require('promise');

// GET /api/friends/saveUserFriend/:userId/:friendId
router.get(
    '/saveUserFriend/:userId/:friendId',
    wrapResponse((request, response) => {
        const friendId = request.params.friendId;
        const userId = request.params.userId;
        if(friendId && userId && Number.isInteger(Number(userId))) {
            request.pool
                .query(db.queries.insert('friends', "communication", {child_id: userId, friend_id: friendId}))
                .then(() => response.status(201).json({ message: "Данные друга сохранены", friendId: friendId }));
        } else {
            return response.status(400).json({ message: "Can not get user with id: " + userId });
        }
    })
);

// DELETE /api/friends/deleteUserFriend/:userId/:friendId
router.delete(
    '/deleteUserFriend/:userId/:friendId',
    wrapResponse((request, response) => {
        const friendId = request.params.friendId;
        const userId = request.params.userId;
        if(friendId && userId && Number.isInteger(Number(userId))) {
            request.pool
                .query(db.queries.delete('friends', "communication", {child_id: userId, friend_id: friendId}))
                .then(() => response.status(201).json({ message: "Данные друга удалены", friendId: friendId }));
        } else {
            return response.status(400).json({ message: "Can not get user with id: " + userId });
        }
    })
);

// GET /api/friends/findInFriendsLocation/:userId
router.get(
    '/findInFriendsLocation/:parentId/:childId',
    wrapResponse((request, response) => {
        const childId = request.params.childId;
        const parentId = request.params.parentId;
        if(parentId && childId) {
            request.pool
                .query(db.queries.select('friends', "communication", {child_id: childId}))
                .then((rows) => {
                    let promises = [];
                    let friends = db.getAll(rows);
                    let coordinates = [];
                    friends.forEach(f => {
                        if(friends && Array.isArray(friends)){
                            promises.push(request.pool.query(db.queries.getChildLastCoordinatesByGPS(f.friend_id))
                                .then((rows) => {
                                    coordinates = coordinates.concat(db.getAll(rows)
                                        .map(c => c.Get_Last_Coordinates_byGPS_simple)
                                        .map(c => { return {lat: c.split(",")[0].replace("(", ""), long: c.split(",")[1]} } ));
                                }))
                        }
                    })
                    return Promise.all(promises).then(() => {
                        let avgLat = 0, avgLong = 0;
                        coordinates.forEach(c => {
                            avgLat += Number(c.lat);
                            avgLong += Number(c.long);
                        });
                        let coord = {lat: avgLat/coordinates.length, long: avgLong/coordinates.length };
                        return coord.lat && coord.long ? coord : null;
                    });
                })
                .then((result) => response.json(result));
        } else {
            return response.status(400).json({ message: "Can not get user with id: " + userId });
        }
    })
);

module.exports = router;