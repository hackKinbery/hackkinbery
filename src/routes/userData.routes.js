const {Router} = require('express');
import {db, wrapResponse, isMacAddress} from '../utils';

const router = Router();

// /api/userData/saveUserData
router.post(
    '/saveUserData',
    wrapResponse((request, response) => {
        const userMeta = request.body;
        if (!userMeta.phone) userMeta.phone = "";
        if (userMeta && Number.isInteger(Number(userMeta.userId)) && userMeta.mac && isMacAddress(userMeta.mac)) {
            request.pool.connect()
                .then(client => {
                    return client.query(db.queries.insert("user_data", "user_data", {
                            user_id: userMeta.userId,
                            mac_address: userMeta.mac,
                            phone: userMeta.phone
                        })
                    );
                })
                .then(() => response.status(201).json({message: "Данные пользователя сохранены", userId: userMeta.userId}));
        } else {
            return response.status(400).json({
                message: "User data is invalid" +
                    (isMacAddress(userMeta.mac) ? " , its not mac address" : "")
            });
        }
    })
);

router.get(
    '/getUserIdByPhone/:phone',
    wrapResponse((request, response) => {
        const phone = request.params.phone;
        if (phone) {
            request.pool
                .query(db.queries.getUserIdByPhone(phone))
                .then(db.getOne)
                .then((result) => response.json(result));
        } else {
            return response.status(400).json({message: "Can not get user by phone: " + phone});
        }
    })
);

router.get(
    '/getUserIdByMac/:mac',
    wrapResponse((request, response) => {
        const mac = request.params.mac;
        if (mac && isMacAddress(mac)) {
            request.pool
                .query(db.queries.select("user_data","user_data", {mac_address: mac}))
                .then(db.getOne)
                .then((result) => response.json(result));
        } else {
            return response.status(400).json({message: "Can not get user by phone: " + phone});
        }
    })
);

router.get(
    '/getMacById/:parentId/:childId',
    wrapResponse((request, response) => {
        const parentId = request.params.parentId;
        const childId = request.params.childId;
        if (parentId && childId) {
            request.pool
                .query(db.queries.getMacById(parentId, childId))
                .then(db.getOne)
                .then((result) => response.json(result));
        } else {
            return response.status(400).json({message: "Can not getMacById: ", childId, parentId});
        }
    })
);


// GET /api/userData/getMonitorByParentId/:parentId
router.get(
    '/getMonitorByParentId/:parentId',
    wrapResponse((request, response) => {
        const parentId = request.params.parentId;
        if(parentId) {
            request.pool
                .query(db.queries.select('monitor', "user_data",{parent_id: parentId}))
                .then(db.getAll)
                .then((result) => response.json(result));
        } else {
            return response.status(400).json({ message: "Can not getMonitorByParentId: " + parentId });
        }
    })
);

// PUT /api/userData/getMonitorByParentId/:parentId/:childId
router.put(
    '/saveParentMonitor/:parentId/:childId',
    wrapResponse((request, response) => {
        const parentId = request.params.parentId;
        const childId = request.params.childId;
        if(parentId && childId) {
            request.pool
                .query(db.queries.insert('monitor', "user_data",{parent_id: parentId, child_id: childId}))
                .then(() => response.status(201).json({message: "Данные монитора сохранены", object: {parent_id: parentId, child_id: childId} }));
        } else {
            return response.status(400).json({ message: "Can not saveParentMonitor: " + parentId });
        }
    })
);


module.exports = router;