const {Router} = require('express');
import {db, wrapResponse, isMacAddress, handleDefault} from '../utils';
const router = Router();

router.post(
    '/saveUserLocation',
    wrapResponse((request, response) => {
        const userLocation = request.body;
        console.log(`Сохраняем координаты (${userLocation.lat}, ${userLocation.long}) для пользователя c userId=${userLocation.userId} (уровень батареи - ${userLocation.battery})`)
        if(userLocation && Number.isInteger(Number(userLocation.userId)) && userLocation.long && userLocation.lat  && userLocation.battery) {
            if(!userLocation.date) userLocation.date = "now()";
            else userLocation.date = 'to_timestamp('+userLocation.date+')';
            request.pool.connect()
                .then(client => {
                    return client.query(db.queries.putGpsCoordinates(userLocation)
                    );
                })
                .then(() => response.status(201).json({}))
                .catch((e) => handleDefault(e, response));
        } else {
            return response.status(400).json({ message: "saveUserLocation: location gps is invalid"});
        }
    })
);

router.post(
    '/saveUserLocations',
    wrapResponse((request, response) => {
        const userLocations = request.body;
        userLocations.forEach((userLocation) => {
            if(userLocation && Number.isInteger(Number(userLocation.userId)) && userLocation.long && userLocation.lat  && userLocation.battery) {
                if(!userLocation.date) {
                    userLocation.date = "now()";
                } else {
                    userLocation.date = 'to_timestamp('+userLocation.date+')';
                }

                return request.pool.connect()
                    .then(client => {
                        return client.query(db.queries.putGpsCoordinates(userLocation)
                        );
                    })
                    .then(() => {
                        console.log('success');
                        return response.status(201).json({});
                    })
                    .catch((e) => {
                        console.log('NOT SUCCESS', e);
                        return handleDefault(e, response)
                    });
            } else {
                console.log("ELSE NOT")
                return response.status(400);
            }
        })
    })
);

router.get(
    '/test',
    wrapResponse((request, response) => {
       console.log("test"+ new Date());
        response.status(201).json({})
    })
);

router.get(
    '/saveUserLocation/:userId/:lat/:long/:battery/:date',
    wrapResponse((request, response) => {
        const userId = request.params.userId;
        const lat = request.params.lat;
        const long = request.params.long;
        const battery = request.params.battery;
        let date = request.params.date;
        if(userId && Number.isInteger(Number(userId)) && lat && long && Number.isInteger(Number(battery))) {
            if(!date) date = "now()";
            else date = 'to_timestamp('+date+')';
            let userLocation = { userId , lat , long, date, battery };
            request.pool.connect()
                .then(client => {
                    return client.query(db.queries.putGpsCoordinates(userLocation)
                    );
                })
                .then(() => response.status(201).json({}))
                .catch((e) => handleDefault(e, response));
        } else {
            return response.status(400);
        }
    })
);

router.get(
    '/saveUserLocation/:userId/:lat/:long/:battery',
    wrapResponse((request, response) => {
        const userId = request.params.userId;
        const lat = request.params.lat;
        const long = request.params.long;
        const battery = request.params.battery;
        if(userId && Number.isInteger(Number(userId)) && lat && long  && battery) {
            const date = "now()";
            let userLocation = { userId , lat , long, date, battery };
            request.pool.connect()
                .then(client => {
                    return client.query(db.queries.putGpsCoordinates(userLocation)
                    );
                })
                .then(() => response.status(201).json({}))
                .catch((e) => handleDefault(e, response));
        } else {
            return response.status(400).json({ message: "saveUserLocation: location gps is invalid"});
        }
    })
);

router.post(
    '/saveUserLocationByMac',
    wrapResponse((request, response) => {
        const macLocation = request.body;
        console.log(`Сохраняем координаты (${macLocation.lat}, ${macLocation.long}) обнаруженного устройства c UUID=${macLocation.mac}`)
        if(macLocation && macLocation.mac && isMacAddress(macLocation.mac) && macLocation.long && macLocation.lat ) {
            if(!macLocation.date) macLocation.date = "now()";
            request.pool.connect()
                .then(client => {
                    return client.query(db.queries.putMacCoordinates(macLocation)
                    );
                })
                .then(() => response.status(201).json({}));
        } else {
            return response.status(400).json({ message: "putMacCoordinates: location mac is invalid"});
        }
    })
);

router.post(
    '/saveBluetoothLocation',
    wrapResponse((request, response) => {
        const userLocation = request.body;
        if(userLocation && userLocation.long && userLocation.lat  && userLocation.mac && isMacAddress(userLocation.mac)) {
            request.pool.connect()
                .then(client => {
                    return client.query(db.queries.insert("location_bt", "communication", {
                            mac: userLocation.mac,
                            long: userLocation.long,
                            lat: userLocation.lat,
                            data: new Date()
                        })
                    );
                })
                .then(() => response.status(201).json({}))
                .catch((e) => handleDefault(e, response));
        } else {
            return response.status(400).json({ message: "location bt mac is invalid" +
                    (isMacAddress(userLocation.mac) ? ", its not mac address" : "" ) });
        }
    })
);

router.get(
    '/getBluetoothLocations/:mac',
    wrapResponse((request, response) => {
        const mac = request.params.mac;
        if(mac && isMacAddress(mac)) {
            request.pool
                .query(db.queries.select('location_bt', "communication"))
                .then(db.getAll)
                .then((result) => response.json(result));
        } else {
            return response.status(400).json({ message: "location bt mac is invalid"});
        }
    })
);

router.get(
    '/getChildTrackByParentId/:parentId/:childId',
    wrapResponse((request, response) => {
        const childId = request.params.childId;
        const parentId = request.params.parentId;
        if(childId && parentId) {
            request.pool
                .query(db.queries.getTrackByGPS(parentId, childId))
                .then(db.getAll)
                .then((result) => response.json(result));
        } else {
            return response.status(400).json({ message: "getChildTrackByParentId is invalid"});
        }
    })
);

router.get(
    '/getTrackByCell/:parentId/:childId',
    wrapResponse((request, response) => {
        const childId = request.params.childId;
        const parentId = request.params.parentId;
        if(childId && parentId) {
            request.pool
                .query(db.queries.getTrackByCell(parentId, childId))
                .then(db.getAll)
                .then((result) => response.json(result));
        } else {
            return response.status(400).json({ message: "getChildTrackByParentId is invalid"});
        }
    })
);

router.get(
    '/getChildLastCoordinatesByGps/:parentId/:childId',
    wrapResponse((request, response) => {
        const childId = request.params.childId;
        const parentId = request.params.parentId;
        if(childId && parentId) {
            request.pool
                .query(db.queries.getLastCoordinatesByGps(parentId, childId))
                .then(db.getAll)
                .then((result) => response.json(result));
        } else {
            return response.status(400).json({ message: "getChildLastCoordinatesByGps is invalid"});
        }
    })
);

router.get(
    '/getLastCoordinatesByBT/:parentId/:childId',
    wrapResponse((request, response) => {
        const childId = request.params.childId;
        const parentId = request.params.parentId;
        if(childId && parentId) {
            request.pool
                .query(db.queries.getLastCoordinatesByBT(parentId, childId))
                .then(db.getAll)
                .then((result) => response.json(result));
        } else {
            return response.status(400).json({ message: "getLastCoordinatesByBT is invalid"});
        }
    })
);

// GET /api/getLastCoordinatesByCell/:parentId/:childId
router.get(
    '/getLastCoordinatesByCell/:parentId/:childId',
    wrapResponse((request, response) => {
        const childId = request.params.childId;
        const parentId = request.params.parentId;
        if(childId && parentId) {
            request.pool
                .query(db.queries.getLastCoordinatesByCell(parentId, childId))
                .then(db.getAll)
                .then((result) => response.json(result));
        } else {
            return response.status(400).json({ message: "getLastCoordinatesByCell is invalid"});
        }
    })
);

router.get(
    '/getChildLastCoordinatesByCell/:childId',
    wrapResponse((request, response) => {
        const childId = request.params.childId;
        const parentId = request.params.parentId;
        if(childId && parentId) {
            request.pool
                .query(db.queries.getChildLastCoordinatesByCell(parentId, childId))
                .then(db.getAll)
                .then((result) => response.json(result));
        } else {
            return response.status(400).json({ message: "getLastCoordinatesByCell is invalid"});
        }
    })
);

router.get(
    '/getLastCoordinatesByGps/:childId',
    wrapResponse((request, response) => {
        const childId = request.params.childId;
        if(childId) {
            request.pool
                .query(db.queries.getChildLastCoordinatesByGPS(childId))
                .then(db.getAll)
                .then((result) => response.json(result));
        } else {
            return response.status(400).json({ message: "getLastCoordinatesByCell is invalid"});
        }
    })
);


module.exports = router;
