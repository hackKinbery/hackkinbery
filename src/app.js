﻿const express = require("express");
const {Pool} = require('pg');
import {getFromConfig, handleDefault, db} from './utils';
import compression from 'compression';

const hostname = process.env.IP_ADDRESS || '0.0.0.0';
const port = 8000;
const app = express();
const savePool = (req, res, next) => {
    req.pool = pool;
    next();
};

let pool = null;
const connectToDataBase = () => {
    pool = new Pool(getFromConfig('postgresql'));
}

const ALLOWED_ORIGINS = [
    'http://35.228.5.164:8000/',
    'http://localhost:63342',
    'http://localhost:19006',
    'http://localhost:19005',
    'http://localhost:19007',
    'http://localhost:80'
];

app.use(function (req, res, next) {
    if(ALLOWED_ORIGINS.indexOf(req.headers.origin) > -1) {
        res.set('Access-Control-Allow-Credentials', 'true')
        res.set('Access-Control-Allow-Origin', req.headers.origin)
    } else { // разрешить другим источникам отправлять неподтвержденные запросы CORS
        res.set('Access-Control-Allow-Origin', '*')
    }

    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, x-ijt, Authorization');


    next();
});

app.use(compression());
app.use(express.json({ extended: true }));
app.use(savePool);
app.use('/api/bluetooth/', require('./routes/bluetooth.routes'));
app.use('/api/location/', require('./routes/location.routes'));
app.use('/api/userData/', require('./routes/userData.routes'));
app.use('/api/request/', require('./routes/request.routes'));
app.use('/api/friends/', require('./routes/friends.routes'));
app.use('/api/monitor/', require('./routes/monitor.routes'));
app.use('/api/childRoutes/', require('./routes/childRoutes.routes'));

app.listen(port, hostname, async () => {
    try {
        await connectToDataBase();
        console.log(`Server running at http://${hostname}:${port}/`);
        // testServer();
    } catch(error) {
        handleDefault(error);
    }
});