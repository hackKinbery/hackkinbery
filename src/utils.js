const sql = require('yesql').pg;
let config = require('config');

export const getFromConfig = (query) => {
    if (config.has(query)) {
        return config.get(query);
    }

    throw new Error(`Error getting "${query}" from config`);
}

// WRAPPERS
export const wrapSql = (queryString, data) => sql(queryString)(data);
export const handleDefault = (error, response) => {
    console.log(error);
    if (response && error && error.message) {
        response.status(500);
    }
};

export const wrapResponse = (func) => {
    return (request, response, next) => {
        try {
            func(request, response, next);
        } catch (error) {
            return handleDefault(error, response);
        }
    }
};


export const isMacAddress = (macAddress) => {
    return true; // deprecated 17:44 /^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/.test(macAddress);
}

export const wrapAccess = (func, accessArray) => {
    return (request, response, next) => {
        func(request, response, next, accessArray);
    };
};
export const getWhere = (data, tableAlias) =>
    data && typeof data === 'object' && Object.keys(data).length
        ? `WHERE ${Object.keys(data).map((key) => `${tableAlias ? `${tableAlias}.` : ''}${key} = :${key}`).join(' AND ')}`
        : '';

const DEFAULT_SCHEMA = "user_data";

// DB HELPERS

export const db = {

    getOne: (result) => result.rows?.[0],
    getAll: (result) => result.rows,
    queries: {
        // SELECT
        select: (table, schema, data, additionalSelect, joins, endQuery, needWhere) => {
            if (!schema) schema = DEFAULT_SCHEMA;
            const queryString = `SELECT t.* ${additionalSelect ? ', ' : ''}${additionalSelect || ''}
                              FROM "${schema}"."${table}" as t ${joins || ''} ${needWhere === false ? '' : getWhere(data, 't')} ${endQuery || ''}`;
            // console.log(queryString);
            return wrapSql(queryString, data);
        },
        // INSERT
        insert: (table, schema, data) => {
            if (!schema) schema = DEFAULT_SCHEMA;
            const queryString = `INSERT INTO "${schema}"."${table}" as t (${Object.keys(data).map((key) => `"${key}"`).join(', ')}) VALUES (${
                Object.keys(data).map((key) => `:${key}`).join(', ')
            }) RETURNING *`;
            return wrapSql(queryString, data);
        },
        // UPDATE
        update: (table, schema, data, whereData) => {
            if (!schema) schema = DEFAULT_SCHEMA;
            const queryString = `UPDATE "${schema}"."${table}" SET ${
                Object.keys(data).map((key) => `${key} = :${key}`).join(', ')
            } ${getWhere(whereData)} RETURNING *`;
            const wrapData = {...data, ...whereData};
            return wrapSql(queryString, wrapData);
        },
        // DELETE
        delete: (table, schema, data) => {
            const queryString = `DELETE FROM "${schema}"."${table}" as t ${getWhere(data, 't')}`;
            return wrapSql(queryString, data);
        },
        putGpsCoordinates: (data) => {
            return wrapSql(`SELECT "communication"."Put_GPS_Coordinates"('${data.userId}', 
            '${data.lat}', '${data.long}',  ${data.date}, '${data.battery}')`, {});
        },
        putMacCoordinates: (data) => {
            return wrapSql(`SELECT "communication"."Put_MAC_Coordinates"('${data.mac}', '${data.date}', 
            '${data.lat}', '${data.long}')`, {});
        },
        getUserIdByPhone: (phone) => {
            return wrapSql(`SELECT "user_data"."Get_ID_byPhone"('${phone}')`, {});
        },
        getTrackByGPS: (parentId, childId) => {
            return wrapSql(`SELECT "communication"."Get_Track_byGPS"('${parentId}','${childId}')`, {});
        },
        getTrackByCell: (parentId, childId) => {
            return wrapSql(`SELECT "communication"."Get_Track_byCell"('${parentId}','${childId}')`, {});
        },
        getMacById: (parentId, childId) => {
            return wrapSql(`SELECT "user_data"."Get_MAC_byID"('${parentId}','${childId}')`, {});
        },
        createRequestToChild: (parentId, childId) => {
            return wrapSql(`SELECT "communication"."Make_request"('${parentId}','${childId}')`, {});
        },
        createRequestBtToChild: (parentId, childId) => {
            return wrapSql(`SELECT "communication"."Make_request_BT"('${parentId}','${childId}')`, {});
        },
        clearRequest: (parentId, childId) => {
            return wrapSql(`SELECT "communication"."Clear_request"('${parentId}','${childId}')`, {});
        },
        clearRequestBt: (parentId, childId) => {
            return wrapSql(`SELECT "communication"."Clear_request_BT"('${parentId}','${childId}')`, {});
        },
        getLastCoordinatesByCell: (parentId, childId) => {
            return wrapSql(`SELECT "communication"."Get_Last_Coordinates_byCell"('${parentId}','${childId}')`, {});
        },
        getChildLastCoordinatesByCell: (childId) => {
            return wrapSql(`SELECT "communication"."Get_Last_Coordinates_byCell_simple"('${childId}')`, {});
        },
        getLastCoordinatesByGps: (parentId, childId) => {
            return wrapSql(`SELECT "communication"."Get_Last_Coordinates_byGPS"('${parentId}','${childId}')`, {});
        },
        getLastCoordinatesByBT: (parentId, childId) => {
            return wrapSql(`SELECT "communication"."Get_Last_Coordinates_byBT"('${parentId}','${childId}')`, {});
        },
        getChildLastCoordinatesByGPS: (childId) => {
            return wrapSql(`SELECT "communication"."Get_Last_Coordinates_byGPS_simple"('${childId}')`, {});
        },

    }
};
